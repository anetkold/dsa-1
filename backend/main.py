import time
import socket
import random
import sys
import struct

# define a multicast options to the socket
def set_socket_options(sr, fromnicip, mcgrpip, mcport):
    try:
        sr.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    except AttributeError:
        pass

    sr.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
    sr.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
    sr.bind((mcgrpip, mcport))

    if fromnicip == '0.0.0.0':
        mreq = struct.pack("=4sl", socket.inet_aton(mcgrpip), socket.INADDR_ANY)
    else:
        mreq = struct.pack("=4s4s", socket.inet_aton(mcgrpip), socket.inet_aton(fromnicip))
    sr.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    return sr

def send_msg(msg, multicast_port, multicast_addr, addr):
    bufsize = 1024
    # Create the socket
    sw = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Make the socket multicast-aware, and set TTL.
    sw.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 20)  
    sw.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(addr))

    # Send the data
    sw.sendto(str(msg).encode('utf-8'), (multicast_addr, multicast_port))
    sw.close()

if __name__ == '__main__':
    IP_ADDRESS = '224.6.6.6'
    NIC_IP = sys.argv[1]
    PORT = 6008
    TIMEOUT = 3
    timeout_counter = 0
    timeout_counter_limit = 2
    vote = random.randint(1, 1024)

    master = ""
    master_prop = ""
    voted = 0
    validating = 0
    votes = {}
    validate = {}
    bufsize = 1024

    

    sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM, proto=socket.IPPROTO_UDP)
    sr = set_socket_options(sock, NIC_IP, IP_ADDRESS, PORT)

    print('My IP address and listening port is: ' + str(NIC_IP) + ":" + str(PORT))
    sr.settimeout(TIMEOUT)
    while True:
        try:
            buf, senderaddr = sr.recvfrom(bufsize)
            msg = buf.decode()
            msg = msg.split("|")

            # if mesage is valid
            if str(msg[0]) == "vote":
                print("Recieving vote " + str(msg[1]) + " from " + str(senderaddr[0]))
                votes[senderaddr[0]] = msg[1]
            if str(msg[0]) == "validate":
                print("Recieving request to validate new master " + str(msg[1]) + " from " + str(senderaddr[0]))
                validate[senderaddr[0]] = msg[1]
            # reset to default if reset msg is recieved... new vote will be send
            if str(msg[0]) == "reset":
                master = ""
                master_prop = ""
                voted = 0
                validating = 0
                votes.clear()
                validate.clear()
            # if message was recieved, reset counter
            timeout_counter = 0
        except socket.timeout:
            if master == "" and validating == 0 and voted < 3 and master_prop == "":
                # send node vote to everyone
                print("Sending my vote: " + str(vote))
                send_msg("vote|" + str(vote), PORT, IP_ADDRESS, NIC_IP)
                voted += 1

            if master == "" and voted == 3 and timeout_counter >= timeout_counter_limit and validating == 0 and master_prop == "":
                print("Analyzing votes...")
                vote_highest = 0
                for ip in list(votes.keys()):
                    vote_selected = int(votes.get(str(ip)))
                  
                    if int(vote_highest) <= int(vote_selected):
                    
                        vote_highest = int(vote_selected)
                        master_prop = str(ip)
                     
                print("The highest vote " + str(vote_highest) + " recieved from " + str(master_prop)) 
                # purge all records in dictionary
                votes.clear()
                voted = 0

            if master == "" and voted < 3 and timeout_counter >= timeout_counter_limit and validating == 0 and master_prop != "":   
                print("Sending validation request...")
                send_msg("validate|" + str(master_prop), PORT, IP_ADDRESS, NIC_IP)
                voted += 1
                if voted == 3:
                    validating = 1

            if master == "" and voted == 3 and validating == 1 and master_prop != "":
                print("Validating results...")
                for master_validation in list(validate.keys()):
                    if validate.get(str(master_validation)) == master_prop:
                        # remove key and value if is same as my vote result
                        validate.pop(str(master_validation))
                    else:
                        print("Votes does not match, the election process will restart!")
                        send_msg("reset|", PORT, IP_ADDRESS, NIC_IP)
                # if the dictionary is empty, write the new master
                master = str(master_prop)
                validating = 0
                master_prop = ""

            if master != "" and voted == 3 and validating == 0 and master_prop == "":
                print("The newly elected master is: " + master)
                timeout_counter = 0
                continue
            timeout_counter += 1
            print("Waiting...")
    sr.close()
