#!/bin/bash
#python script must be started with -u parameter as unbuffered output!
NET_IP=$(hostname -I | awk '{print $1}')
sleep 1
python3 -u ./main.py $NET_IP &
/bin/bash -c "trap : TERM INT; sleep infinity & wait" 
