**Zadání**

Zadáním první semestrální práce na předmět KIV/DSA bylo implementovat
distribuovaný algoritmus pro volbu 1z N, který využívá multicast UDP
zpráv.

Algoritmus nemusí být odolný vůči výpadkům, ale v konečném čase musí
dojít ke správnému výběru jednoho z N uzlů.

**Implementace:**

K implementaci bylo využito nástrojů **Vagrant** a **Docker.** Aplikace
byla programována v jazyce **Python**.

Jedná se o jeden skript, který nejprve otevře dva multicastové sockety
\-- jeden pro čtení, druhý pro zápis. Poté v nekonečné smyčce přijímá
zprávy od ostatních uzlů, na kterých běží stejný skript, a dle prvního
slova ve zprávě se rozhodně o akci, kterou skript následně vykoná.

Mohou nastat následující možnosti:

-   *Vote:*

    -   Přijetí hlasu od některého z ostatních uzlů.

-   *Validate:*

    -   Uzel tímto žádá o ověření výsledků volby.

-   *Reset:*

    -   Po tomto typu zprávy se proces volby jednoho z N restartuje a
začne nanovo.

Pokud master nebyl zvolený ani není žádný navržený, který by se ověřoval
a zároveň se nevalidují hlasy ostatních a nebyla ukončena volb, tak
následně odešle každý uzel třikrát svůj hlas.

Jestliže každý uzel třikrát volil (nebo nevypršel časový limit pro
přijmutí zprávy), tak se spustí analýza přijatých hlasů (nejvyšší
hodnota určuje potenciálního mastera).

Potom, co uzel zjistí maximální hodnotu z hlasů, odešle zprávu s
požadavkem na porovnání výsledků s ostatními. Od této chvíle žádný z
uzlů nemůže odeslat svůj hlas.

Nakonec každý z uzlů porovná příchozí výsledky se svojí zjišťenou
maximální hodnotou.

V případě shody prohlásí uzel s nejvyšší hodnotou za mastera. Jestliže
však hodnoty nesouhlasí, posílá se reset zpráva, která vyvolá nové
hlasování.

**Sestavení**

K sestavení aplikací slouží příkaz '**vagrant up'**, který z přítomného
souboru 'Dockerfile' vytvoří strukturu, kterou následně také spustí. Pro
sledování průběhu volby lze použít příkaz **'watch docker logs APP-X'**,
kde 'X' je číslo sledovaného uzlu. Alternativně se lze připojit na konzoli běžícího 
kontejneru pomocí příkazu **'docker exec -it APP-X bash'**, kde 'X' je opět číslo uzlu.

Aplikace na výstup vypisuje informace o jednotlivých krocích a na závěr
vypíše výsledek volby.

